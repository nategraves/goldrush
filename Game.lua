local Game = {}

physics = require('physics')
physics.start()
physics.setGravity(0, 10)
--physics.setDrawMode('hybrid')

function Game.new()

	local game = display.newGroup()
	game.id = "game"

	-- local soundBackground = audio.loadStream("something.mp3")
	-- local soundBackgroundChannel

	-- modules
	local Mound = require('Mound')
	local Guy = require('Guy')

	--stuff to display
	local background, scoreText, reset, guy

	--some groups
	local mounds

	--variables
	local score = 0

	--functions
	local initScore, initReset, initBackground, initGuy, initMounds, startListeners, stopListeners, moveGuy

	function initScore()
		scoreText = display.newText(game, "Your Score: " .. score, 10, 10, "AP", 24)
	end

	function initBackground()
		local sprite = require('sprite')
		local riverSheet = sprite.newSpriteSheet('assets/river.png', 150, 119)

		local riverSet = sprite.newSpriteSet(riverSheet, 1, 5)
		sprite.add( riverSet, 'river', 1, 5, 800, 0)
		local river = sprite.newSprite(riverSet)
		river.x = 100; river.y = 261
		river:toBack()
		river:prepare('river')
		river:play()

		background = display.newImage('assets/background-01.png', 0, 0)
		background:toBack()
	end

	function initReset()
		reset = display.newText(game, "More Mounds", 350, 10, native.systemFont, 16)
	end

	function initGuy()
		guy = Guy.new()
	end

	function initMounds()
		mounds = display.newGroup()
		game:insert(mounds)
		for i = 1, 5 do
			local initY = 180
			local mound
				mound = Mound.new(math.random(200, 410), initY + (i * 20))
			mounds:insert(mound)
		end
	end

	function startListeners()
		background:addEventListener("tap", moveGuy)
		reset:addEventListener("tap", moreMounds)
	end

	function stopListeners()
		background:removeEventListener("tap", moveGuy)
		reset:removeEventListener("tap", moreMounds)
	end

	function moreMounds()
		if mounds.numChilden == 0 then
			initMounds()
		end
	end

	function moveGuy(event)
		--print(event.target)
		local xdiff = guy.x - event.x
		local ydiff = guy.y - event.y
		
		if math.abs(xdiff) < 20 then
			if ydiff < 0 then
				guy:prepare('walk-down')
				guy:play()
			else
				guy:prepare('walk-up')
			end
		else 
			if xdiff < 0 then
				guy:prepare('walk-right')
				guy:play()
			else
				guy:prepare('walk-left')
				guy:play()
			end
		end
		

		local newy = event.y
		if newy < 180 then
			newy = 180
		end

		transition.to(guy, { time=1000, x=event.x, y=newy, transition = easing.outQuad, onComplete=endMove })
	end

	function endMove()
		guy:prepare('idle')
		guy:play()
		return true
	end

	function setScore(amount)
		score = score + amount
		scoreText.text = "Your Score: " .. score
	end

	function game:startGame()
		initScore()
		initBackground()
		initReset()
		initGuy()
		initMounds()
		startListeners()
		--soundBackgroundChannel = audio.play(soundBackground, {loops = -1})
	end

	return game

end

return Game