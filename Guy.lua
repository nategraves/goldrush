local Guy = {}

function Guy.new()
	local sprite = require('sprite')
	local guySheet = sprite.newSpriteSheet('assets/guy-walking.png', 60, 80)

	local guySet = sprite.newSpriteSet(guySheet, 1, 10)
	sprite.add( guySet, "idle", 9, 2, 1200, 0)
	sprite.add( guySet, "walk-right", 1, 2, 700, 0)
	sprite.add( guySet, "walk-left", 3, 2, 700, 0)
	sprite.add( guySet, "walk-down", 5, 2, 700, 0)
	sprite.add( guySet, "walk-up", 7, 2, 700, 0)

	local guy = sprite.newSprite(guySet)
	guy:prepare('idle')
	guy:play()
	guy.x, guy.y = 20, 200
	guy.dirt = 0

	return guy

end

function addDirt(dirtAdded)
	guy.dirt = guy.dirt + dirtAdded
	setScore(guy.dirt)
end

return Guy