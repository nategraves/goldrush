local Guy = require('Guy')

local Mound = {}

function Mound.new(moundX, moundY)

	assert(moundX or moundY, "Required parameter missing")

	local sprite = require('sprite')
	local moundSheet = sprite.newSpriteSheet('assets/mound.png', 80, 55)

	local moundSet = sprite.newSpriteSet(moundSheet, 1, 2)
	sprite.add( moundSet, 'full', 1, 1, 15000, 0)
	sprite.add( moundSet, 'medium', 2, 1, 15000, 0)
	sprite.add( moundSet, 'small', 3, 1, 15000, 0)
	local mound = sprite.newSprite(moundSet)
	mound:prepare('full')
	mound:play()

	mound.id = 'mound'
	mound.x = moundX
	mound.y = moundY
	mound:toFront()
	mound.dirt = math.random(21.0,45.0)
	mound.halfDirt = mound.dirt / 2
	mound:addEventListener("touch", dig)

	return mound

end

function dig(event)
	if event.phase == 'ended' then

		if event.target.dirt > 0 then

			if event.target.dirt < event.target.halfDirt then
				event.target:prepare('medium')
				event.target:play()
			end

			----------Make some dirt fly----------
			local dirt = display.newRect(event.x, event.y, 10, 10)
			dirt:setFillColor(64, 48, 0)
			dirt.strokeWidth = 3
			dirt:setStrokeColor(44, 28, 0)
			physics.addBody(dirt)
			
			local dirtAdded = display.newText('+1', event.x, event.y, native.systemFont, 10)
			physics.addBody(dirtAdded)
			setScore(1)
			event.target.dirt = event.target.dirt - 1
		else
			event.target:removeEventListener("tap", event.target)
			event.target:removeSelf()
			event.target = nil
		end
	end
end

return Mound