-----------------------------------------------------------------------------------------
--
-- menu.lua
--
-----------------------------------------------------------------------------------------

--[[
module(..., package.seeall)

function new()
	local localGroup = display.newGroup()

	local background = display.newImage("assets/background-gold.png", 0, 0)
	local logo = display.newImage("assets/logo-gold.png", 150, 60)
	logo.scene = "Game"

	localGroup:insert(background)
	localGroup:insert(logo)

	logo:addEventListener("touch", changeScene)

	function changeScene(event)
		if event.phase == 'ended' then
			director:changeScene(event.target.scene)
		end
	end

	return localGroup
end
]]